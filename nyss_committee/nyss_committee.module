<?php

/**
 *  @file
 *  NYSS Committee
 *
 *  This module handles information related to committees.
 */

define('NYSS_OPENLEG_PATH', 'http://open.nysenate.gov/legislation/');

/**
 *  What committee does this page belong to?
 *
 *  If a page view or content belongs to a specific senator, this will return
 *  the node for that senator, or FALSE if it doesn't belong to any senator.
 *
 *  @return
 *    The Committees' node, or FALSE.
 */
function nyss_committee() {
  static $committee;
  // Create the static cache for future reference.
  if (is_null($committee)) {
    // Our default state.
    $committee = FALSE;

    if ((arg(0) == 'node' || arg(0) == 'committee') && ($nid = arg(1)) && is_numeric($nid)) {
      $node = node_load($nid);
    }
    elseif (arg(0) == 'committee' && arg(1)) {
      $node = nyss_committee_node_from_title(arg(1));
    }
    $committee = nyss_committee_node($node);
  }
  return $committee;
}

/**
 *  Is the current page a committee's home page?
 *
 *  @return
 *    Boolean
 */
function nyss_committee_home() {
  if (arg(0) == 'node' && ($nid = arg(1)) && is_numeric($nid) && arg(2) == '') {
    $node = node_load($nid);
  }
  elseif (arg(0) == 'committee' && arg(1) && arg(2) == '') {
    $node = nyss_committee_node_from_title(arg(1));
  } else {
    return FALSE;
  }
  return ($node->type == 'committee');  
}

/**
 *  Retrieves the committee node from a given title from a URL.
 *  @param $title
 *    The title from the URL.
 *  @return
 *    The corresponding committee node object.
 */
function nyss_committee_node_from_title($title) {
  static $titles;

  if (!isset($titles[$title])) {
    $titles[$title] = '';
    // If we don't have a match from the field_path, then match for $node->title.
    $nid = db_result(db_query_range(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE REPLACE(REPLACE(LOWER(n.title),'/',''),',','') = '%s' AND n.status = 1"), 
      str_replace(array('_', '-', ',', '/'), array(' ', ' ', '', ''), $title), 0, 1));
    if ($nid) {
      $titles[$title] = node_load($nid);
    }
  }
  return $titles[$title];
}

/**
 *  Turns a committee name into a string suitable for use in a URL path
 *  @param $title
 *    The committee name, e.g., 'Environmental Protection'
 *  @return
 *    The equivalent URL string, e.g., 'environmental-protection'
 */
function nyss_committee_path_name($name) {
  return strtolower(str_replace(array(' ', ',', ''), array('-', '', ''), $name));
}
 
/**
 *  This will return the committee associated with the node.
 *
 *  @param $node
 *    The node referencing the committee, or a committee's node.
 *  @param $reset
 *    (optional) If TRUE, then reset the static variable.
 *  @return
 *    The committee node referenced by the node.
 */
function nyss_committee_node($node, $reset = FALSE) {
  static $committees;

  // Reset our static array.
  if (is_null($committees) || $reset) {
    $committees = array();
  }

  // We cache the committee nodes.
  if (is_null($committees[$node->nid])) {
    $committees[$node->nid] = FALSE;

    if ($node->type == 'committee') {
      // If we're given a committee's node, then return it.
      $committees[$node->nid] = $node;
    }
    else if ($node->field_committee[0]['nid']) {
      // Check if the node references a committee.
      $committee = node_load($node->field_committee[0]['nid']);
      if ($committee->type == 'committee') {
        // If the referenced node is a valid committee, then bingo.
        $committees[$node->nid] = $committee;
      }
    }
  }

  return $committees[$node->nid];
}

/**
 *  Return the link to display to OpenLeg calendar info on committee pages.
 */
function nyss_committee_openleg_meetings_link() {
  $committee = nyss_committee();
  $path = NYSS_OPENLEG_PATH . 'meetings/?' . urlencode($committee->title);
  $display_text = t('View the official list of legislative meetings for this committee');
  return '<div class="openleg-meetings-link">' . l($display_text, $path) . '</div>';
}

/**
 *  Return the next event associated with a committe.
 *
 *  @param $committee
 *    The committee node
 *  @return
 *    The next event 
 */
function nyss_committee_next_event($nid) {
  $date = date_create('now', date_default_timezone());
  $date_string = substr(date_format($date, DATE_ISO8601), 0, 16);
  $sql = "SELECT DISTINCT(node.nid) AS nid,
      node.created AS node_created
    FROM {node} node 
    LEFT JOIN {content_field_committee} node_data_field_committee ON node.vid = node_data_field_committee.vid
    LEFT JOIN {node} node_node_data_field_committee ON node_data_field_committee.field_committee_nid = node_node_data_field_committee.nid
    LEFT JOIN {content_type_event} node_data_field_date ON node.vid = node_data_field_date.vid
    WHERE ((node.type in ('event')) AND (node.status <> 0) AND (node_node_data_field_committee.nid = %d))
    AND ((DATE_FORMAT(ADDTIME(STR_TO_DATE(node_data_field_date.field_date_value, '%Y-%m-%%dT%T'), SEC_TO_TIME(-18000)), '%Y-%m-%%d') >= '%s') 
     OR (DATE_FORMAT(ADDTIME(STR_TO_DATE(node_data_field_date.field_date_value2, '%Y-%m-%%dT%T'), SEC_TO_TIME(-18000)), '%Y-%m-%%d') >= '%s'))
    ORDER BY node_created DESC LIMIT 1";
  $result = db_result(db_query($sql, $nid, $date_string, $date_string));
  return $result;
}

/**
 *  Return the current live video source for a committee.
 *
 *  @param $nid
 *    The committee node ID
 *  @return
 *    A  
 */
function nyss_committee_live_video_source($nid) {
  $next_event_nid = nyss_committee_next_event($nid);
  $next_event_status = nyss_event_status($next_event_nid);
  if ($next_event_status['when'] == 'during') {
    if (nyss_videosettings_is_live($next_event_nid)) {
      return 'event';
    }
    else if (nyss_videosettings_is_live($nid)) {
      return 'committee';
    }
  }
  return 'none';
}
