<?php

/**
 *  @file
 *  NYSS Blocks
 *
 *  This module defines several custom blocks.
 *
 *  To add a new block, you'll need to do the following:
 *    1) Add the block to the $deltas list in nys_blocks_block $op 'list',
 *        keyed as $delta => $info_title.
 *    2) Define when the block will appear, in nyss_blocks_display.
 *    3) Create a theme file at themes/nyss_blocks.theme.block.[$delta].inc
 *    4) Create two functions in that file: theme_nyss_blocks_view_subject_[$delta]
 *        and theme_nyss_blocks_view_content_[$delta].
 *    5) Rebuild your theme to catch the new functions.
 */

/**
 *  Implement hook_menu().
 */
function nyss_blocks_menu() {
  $items = array();
  $items['admin/settings/nyss_blocks/suppress'] = array(
    'title' => 'Suppress all blocks',
    'description' => 'Suppress display of all sidebar blocks on the specified pages.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nyss_blocks_suppress'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Form builder. Configure settings for specifying which pages should suppress all blocks
 * so that the full width of the web page is available for content.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function nyss_blocks_suppress() {
  $description = t("Enter one page per line as Drupal paths for each page where you want to suppress all sidebar blocks so that the full width of the web page is available for content. The '*' character is a wildcard. Example paths are %path and %path-wildcard for every calendar page.", array('%path' => 'blog', '%path-wildcard' => 'calendar/*'));
  $form['nyss_blocks_suppress'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('nyss_blocks_suppress', ''),
    '#description' => $description,
  );
  return system_settings_form($form);
}

/**
 *  Implements hook_block().
 */
function nyss_blocks_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
     case 'list':
      $deltas = array(
        'happening_now' => t('Happening Now...'),
        'senate_blogs' => t('Senate Blogs'),
        'senator_tabs' => t('Senator tabs'),
        'most_popular' => t('Most Popular'),
        'front_carousel' => t('Front Carousel'),
        'issues' => t('Issues'),
        'ustream_video' => t('Live Stream'),
        'search_blogs' => t('Search All Blogs'),
        'committee_chair' => t('Committee Chair'),
        'committee_members' => t('Committee Members'),
        'committee_issues' => t('Committee Issues'),
        'committees' => t('Committees'),
        'about_senator_blog' => t('About this blog'),
        'nyss_senator_search' => t('Senator Page Senator Search'),
        'global_nyss_senator_search' => t('Global Senator Search'),
        'nyss_signup' => t('Signup'),
        'social_buttons' => t('Social buttons'),
        'my_committees' => t('My Committees'),
        'senator_carousel' => t('Senator\'s Carousel'),
        'legislation_bookblock' => t('Legislation Bookblock'),
        'legislation_navigator' => t('Legislation Navigator'),
        'bill_search' => t('Search by Bill Number'),
        'live_session' => t('Watch Live Session'),
        'live_session_temp' => t('Watch Live Session temp'),
        'sen_video' => t('Senate Video Services'),
        'flickr_gallery' => t('Image Gallery'),
        'group_links' => t('Group Links'),
        'group_twitter' => t('Group Twitter Stream'),
        'senator_promo_sidebar' => t('Senator Sidebar Promo'),
        'senator_promo_bottom' => t('Senator Bottom Promo'),
        'senator_homepage_extensible' => t('Senator Homepage Extensible Content Area'),
        'initiative_sidebar_html' => t('Custom Sidebar HTML for Initiatives'),
      );
      $blocks = array();
      foreach ($deltas as $delta => $info) {
        $blocks[$delta] = array(
          'info' => $info,
          'visibility' => 2,
          'pages' => "<?php return nyss_blocks_display('$delta'); ?>",
        );
      }
      return $blocks;
    case 'configure':
      if ($delta == 'social_buttons') {
        require_once('themes/nyss_blocks.theme.block.social_buttons.inc');
        return nyss_blocks_configure_form_social_buttons();
      }
      break;
    case 'save':
      if ($delta == 'social_buttons') {
        require_once('themes/nyss_blocks.theme.block.social_buttons.inc');
        nyss_blocks_configure_form_save_social_buttons($edit);
      }
      break;
    case 'view':
      return array(
        'subject' => theme('nyss_blocks_view_subject_'. $delta),
        'content' => theme('nyss_blocks_view_content_'. $delta),
      );
  }
}

/**
 *  Determine whether to display a specific block on the page.
 *
 *  This function is called by the 'pages' section of each individual block.
 *  This is defined in hook_block, and controlled by PHP visibility (2).
 *
 *  @param $delta
 *    The block to display.
 *  @return
 *    TRUE or FALSE, depending on whether the block should be displayed on this page.
 */
function nyss_blocks_display($delta=NULL) {
  // suppress all blocks on pages that match the list on the settings page
  $path = drupal_get_path_alias($_GET['q']);
  // Compare with the internal and path alias (if any).
  $page_match = drupal_match_path($path, variable_get('nyss_blocks_suppress', ''));
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], variable_get('nyss_blocks_suppress', ''));
  }
  if ($page_match) return FALSE;
  if ((arg(0) == 'video' || arg(0) == 'video_nothumbs') && $delta != 'sen_video') {
    return FALSE;
  }
  switch ($delta) {
    case 'user:1':
    case 'devel:1':
    case 'devel:2':
    case 'nyss_signup':
      $path = strtolower(nyss_blocks_get_path());
      if ((arg(0) == 'districts') && (arg(1) == 'map')) {
        return FALSE;
      }
      if ($path == 'issues-legislation' && arg(1) == 'search') {
        return FALSE;
      }
      if ($delta == 'nyss_signup' && arg(0) == 'nyss_signup') {
        return FALSE;
      }
      if (in_array($path, array('opendata','global_report','calendar','nyss/user','search/advanced', 'census/district/maps'))) {
        return FALSE;
      }
      if ($delta == 'nyss_signup') {
        $senator = nyss_senator();
        if ($senator->type == 'senator') {
          return (!nyss_blocks_is_checked($senator->field_hide_updates_signup));
        }
      }
      return TRUE;
    // Show some blocks on senator's pages unless the senator has checked the "hide" option for that block
    case 'district_map':
      $senator = nyss_senator();
      return ($senator->type == 'senator' && !nyss_blocks_is_checked($senator->field_hide_district_map));
    case 'senator_featured_video':
      $senator = nyss_senator();
      return ($senator->type == 'senator' && !nyss_blocks_is_checked($senator->field_hide_featured_video));
    case 'upcoming_events_senate':
      $senator = nyss_senator();
      return ($senator->type == 'senator' && !nyss_blocks_is_checked($senator->field_hide_upcoming_events));
    case 'tax_credit_calculator':
      $senator = nyss_senator();
      // This one returns a value of true if the check box IS checked.
      return ($senator->type == 'senator' && nyss_blocks_is_checked($senator->field_show_tax_credit_calculator));
    case 'global_nyss_senator_search':
      $path = strtolower(nyss_blocks_get_path());
      if ((arg(0) == 'districts') && (arg(1) == 'map')) {
        return FALSE;
      }
      if ($path == 'issues-legislation' && arg(1) == 'search') {
        return FALSE;
      }
      if (in_array($path, array('opendata','global_report','calendar','nyss/user','search/advanced', 'census/district/maps'))) {
        return FALSE;
      }
      $senator = nyss_senator();
      return empty($senator);
    case 'front_carousel':
    case 'happening_now':
    case 'senate_blogs':
      return drupal_is_front_page() && arg(0) == 'front';
    case 'most_popular':
//      $senator = nyss_senator();
//      return (drupal_is_front_page() || ($senator->nid == arg(1) && $senator->type == 'senator' && arg(2) != 'edit'));
      return (drupal_is_front_page());
    case 'social_buttons':
      if (arg(0) == 'node' && is_numeric(arg(1))) {
        $node = node_load(arg(1));
        if ($node->type == 'group') return TRUE;
      }
      else if (drupal_is_front_page()) return TRUE;
    case 'nyss_senator_search':
      $senator = nyss_senator();
      return (!empty($senator));
    case 'senator_tabs':
    case 'senator_carousel':
    case 'senator_bottom':
      $senator = nyss_senator();
      return ($senator->nid == arg(1) && $senator->type == 'senator' && arg(2) != 'edit');
    case 'from_my_committee':
      $senator = nyss_senator();
      return ($senator->nid == arg(1) && $senator->type == 'senator' && arg(2) != 'edit' && $senator->field_committee_updates[0]['value'] != 1);
    case 'my_committees':
      $senator = nyss_senator();
      return ($senator->nid == arg(1) && $senator->type == 'senator' && arg(2) != 'edit' && $senator->field_hide_committees[0]['value'] != 1);
    case 'issues':
    case 'committees':
      $path = nyss_blocks_get_path();
      return ($path == 'issues-legislation' && (arg(1) != 'search'));
    case 'ustream_video':
      $path = nyss_blocks_get_path();
      return (($path == 'issues-legislation' || $path == 'media_reports') && (arg(1) != 'search'));
    case 'sen_video':
      $senator = nyss_senator();
      $arg0 = arg(0);
      $arg1 = arg(1);
      if ($arg0 == 'node' && ($nid = $arg1) && is_numeric($nid)) {
        $node = node_load($nid);
      }
      if ($senator && (arg(2) == 'gallery' || arg(2) == 'video')) {
        return ($senator->field_hide_senate_videos[0]['value'] != 1);
      }
      else {
        return ((arg(0) == 'media' || arg(0) == 'video' || arg(0) == 'video_nothumbs' || arg(2) == 'gallery' || arg(2) == 'video' || ($node->type == 'video')));
      }
    case 'committee_updates':
      return nyss_senator_committee_home();
/*      $committee = nyss_senator_committee();
      return ($committee->nid == arg(1) && $committee->type == 'committee' && arg(2) != 'edit'); */
    case 'committee_chair':
      $committee = nyss_senator_committee();
      return ($committee != FALSE && $committee->field_chairs[0]['nid'] != '');
    case 'committee_members':
    case 'committee_issues':
    case 'committee_related_pages':
      $committee = nyss_senator_committee();
      return ($committee != FALSE);
    case 'initiative_updates':
      return nyss_initiative_home();
    case 'search_blogs':
      $arg0 = arg(0);
      $arg1 = arg(1);
      if ($arg0 == 'node' && ($nid = $arg1) && is_numeric($nid)) {
        $node = node_load($nid);
      }
      return ($node->type == 'blog' || ($arg0 == 'senator' && arg(2) == 'blog'));
    case 'about_senator_blog':
      if (arg(0) == 'node' && ($nid = arg(1)) && is_numeric($nid)) {
        $node = node_load($nid);
        if($node->type == 'senator' && $node->field_hide_about[0]['value'] != 1) {
          return TRUE;
        }
        else if ($node->type == 'group') {
          return TRUE;
        }
      }
      return (arg(0) == 'blog');
    // The following 'views' blocks are not defined with this module,
    // but for convenience are used to determine their display options.
    case 'views_gallery_block_1':
      $senator = nyss_senator();
      return ($senator && arg(2) == 'gallery');
    case 'live_session':
    case 'live_session_temp':
      return (drupal_is_front_page());
    case 'views_senator_press_block_1':
      $senator = nyss_senator();
      return ($senator && (arg(2) == 'newsroom') );
    case 'legislation_navigator':
    case 'legislation_bookblock':
      if (arg(0) == 'node' && ($nid = arg(1)) && is_numeric($nid)) {
        $node = node_load($nid);
      }
      return ($node->type == 'outline');
    case 'bill_search':
      $path = nyss_blocks_get_path();
      return ($path == 'issues-legislation' && (arg(1) != 'search'));
    case 'flickr_gallery':
      return (arg(0) == 'node' && (arg(1) == 15 || arg(1) == 21915));
    case 'group_links':
    case 'group_twitter':
      if(arg(0) == 'node' && (is_numeric(arg(1)))) {
        $node = node_load(arg(1));
        return ($node->type == 'group' || $node->type == 'initiative' || ($node->field_initiative[0]['nid'] != '') || count($node->og_groups) > 0);
      }
      else {
        return FALSE;
      }
    case 'senator_promo_sidebar':
      $node = nyss_senator_committee();
      if (!$node) {
        $node = nyss_senator();
      }
      return(($node->nid == arg(1) && ($node->type == 'senator' || $node->type == 'committee') && arg(2) != 'edit') && $node->field_promo_sidebar[0]['filepath']);
    case 'senator_promo_bottom':
      $node = nyss_senator_committee();
      if (!$node) {
        $node = nyss_senator();
      }
      return(($node->nid == arg(1) && ($node->type == 'senator' || $node->type == 'committee') && arg(2) != 'edit') && $node->field_promo_bottom[0]['filepath']);
    case 'flag_senator_featured_poll':
      $senator = nyss_senator();
      return (($senator->nid == arg(1) && $senator->type == 'senator' && arg(2) != 'edit') && ($senator->field_quickpoll_settings[0]['value'] == 1 || $senator->field_quickpoll_settings[0]['value'] == 3));
    case 'senator_homepage_extensible':
      $senator = nyss_senator();
      return (($senator->nid == arg(1) && $senator->type == 'senator' && arg(2) != 'edit'));
    case 'initiative_sidebar_html':
      $path = nyss_blocks_get_path();
      if(arg(0) == 'node' && (is_numeric(arg(1)))) {
        $node = node_load(arg(1));
        return ( ($path != 'census/district/maps') && ($node->type == 'initiative' || ($node->field_initiative[0]['nid'] != '')));
      }
      else {
        return FALSE;
      }
    case 'masquerade':
      return TRUE;
  }
  return FALSE;
}

/**
 * Tests whether a checkbox field is checked.
 *
 *  @param $field
 *    The field.
 * @return Boolean
 */
function nyss_blocks_is_checked($field) {
	return ($field[0]['value']);
}

/**
 * Gets current path alias.
 *
 * @return string
 */
function nyss_blocks_get_path() {
  static $path;
  if (empty($path)) {
    $path = drupal_get_path_alias($_GET['q']);
  }
  return $path;
}

/**
 *  Implements hook_theme().
 */
function nyss_blocks_theme($existing, $type, $theme, $path) {
  $items = array();
  foreach (nyss_blocks_block('list') as $delta => $block) {
    $items["nyss_blocks_view_subject_$delta"] = array(
      'arguments' => array(),
      'file' => "themes/nyss_blocks.theme.block.$delta.inc",
    );
    $items["nyss_blocks_view_content_$delta"] = array(
      'arguments' => array(),
      'file' => "themes/nyss_blocks.theme.block.$delta.inc",
    );
  }
  return $items;
}

/**
 * Implements hook_form_alter().
 */
function nyss_blocks_form_alter(&$form, $form_state, $form_id) {
  // Remove the block title setting for the social buttons block.
  if ($form['module']['#value'] == 'nyss_blocks' && $form['delta']['#value'] == 'social_buttons') {
    $form['block_settings']['title'] = '';
    $form['block_settings']['title']['#type'] = 'hidden';
  }
}
