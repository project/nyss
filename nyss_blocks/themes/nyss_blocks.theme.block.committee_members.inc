<?php

/**
 *  @file
 *  Theme functions for the 'Committee Members' block
 */

/**
 *  Subject of the 'Committee Members' block.
 *
 *  @return
 *    The subject of that block.
 */
function theme_nyss_blocks_view_subject_committee_members() {
  $committee = nyss_senator_committee();
  
  if($committee->field_committee_members[0]['value']) {
    return '';
  }
  else {
    return t('Committee Members');
  }
}

/**
 *  Display the content of the 'Committee Members' block.
 *
 *  @return
 *    The content of this block.
 */
function theme_nyss_blocks_view_content_committee_members() {
  $committee = nyss_senator_committee();
  $output = '<h2 class="committee_name">' . $committee->title . '</h2>';
//  $output .= '<div class="committee-description">' . $committee->body . '</div>';
  $output .= '<h2 class="title block_title">' . t('Committee members') . '</h2>';
  // Add chair or chairpersons
  if($committee->field_pseudochair[0]['value'] != 1) {
    foreach ($committee->field_chairs as $field_chair) {
      $chair = node_load($field_chair['nid']);
      $district = db_result(db_query('SELECT n.title FROM {node} n WHERE n.nid = %d', $chair->field_senators_district[0]['nid']));
      $output .= '<div class="committee-chair">';
      $output .= '<a class="picture" href="'. url('node/'. $chair->nid) .'">';
      $output .= theme('imagecache', 'senator_teaser', $chair->field_profile_picture[0]['filepath'], $chair->title, $chair->title, NULL);
      $output .= '</a>' . '<label>' . t('Chair: ') . '</label>' . '<a href="'. url('node/'. $chair->nid) .'">Sen. ' . $chair->title . '</a><br />';
      $output .= substr($chair->field_party_affiliation[0]['value'], 0, 1) . '-' . strip_tags($district);
      
      // add chairperson contact link
      $contacturl = trim(preg_replace('#[^\p{L}\p{N}]+#u', ' ', $chair->title));
      $contacturl = str_replace(' ', '-', strtolower($contacturl));
      $contacturl = 'senator/'. $contacturl;
      $output .= '<span class="contact"> | '. l(t('Contact'), $contacturl . '/contact').'</span>';

      $output .= theme('nyss_blocks_view_content_social_buttons', $chair);
      $output .= '</div><div style="clear:both;"></div>';
    }
  }

  // add committee members
  $output .= '<div class="committee-members">';
  if(!$committee->field_committee_members[0]['value']) {
    $params = array();
    foreach ($committee->field_multi_senator as $senator) {
      if ($senator['nid']) {
        $params[] = $senator['nid'];
      }
    }
    if (count($params)) {
      $senators = array();
      $query = "SELECT DISTINCT(node.nid) AS nid, node.title AS node_title, node_data_field_last_name.field_last_name_value AS node_data_field_last_name_field_last_name_value 
        FROM {node} node LEFT JOIN {content_type_senator} node_data_field_last_name ON node.vid = node_data_field_last_name.vid 
        WHERE (node.type in ('senator')) AND (node.nid IN (" . implode(', ', $params) . ")) GROUP BY nid ORDER BY node_data_field_last_name_field_last_name_value ASC";
      $result = db_query($query);
      while ($senator = db_fetch_object($result)) {
        $senators[] = l(t('Sen. @name', array('@name' => $senator->node_title)), 'node/'. $senator->nid);
      }
      $output .= theme('item_list', $senators);
    }
  }
  else {
    $output .= $committee->field_committee_members[0]['value'];
  }
  $output .= '</div>';
  return $output;
}
