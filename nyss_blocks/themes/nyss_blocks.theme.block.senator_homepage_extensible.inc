<?php

/**
 *  @file
 *  Theme functions for the 'Senator Homepage Extensible Content' block
 */

/**
 *  Subject of the 'Senator Homepage Extensible Content' block.
 *
 *  @return
 *    The subject of that block.
 */
function theme_nyss_blocks_view_subject_senator_homepage_extensible() {
  return '';
}

/**
 *  Display the content of the 'Senator Homepage Extensible Content' block.
 *
 *  @return
 *    The content of this block.
 */
function theme_nyss_blocks_view_content_senator_homepage_extensible() {
  $senator = nyss_senator();
  if($senator->field_quickpoll_settings[0]['value'] >= 2) {
    $block = module_invoke('views', 'block', 'view', 'd7fe595f16e9bf2209fe69415aba0700');
    $output .= '<div class="block">';
    $output .= '<h2 class="title block_title">'. $block['subject'] .'</h2>';
    $output .= $block['content']; 
    $output .= '</div>';
  }
  return $output;
}
