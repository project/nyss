<?php

/**
 *  @file
 *  NYSS Session
 *
 *  This module provides an API to tell the website whether the senate is in session.
 *
 */

define('NYSS_USE_LDBC_FEED', 0);
define('NYSS_IN_SESSION', 1);
define('NYSS_NOT_IN_SESSION', 2);

/**
 *  Implement hook_menu().
 */
function nyss_session_menu() {
  $items = array();
  $items['admin/settings/nyss_session'] = array(
    'title' => 'Session settings',
    'description' => 'Specify settings to tell the website whether the senate is in session.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nyss_session'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_theme()
 */
function nyss_session_theme() {
  return array(
    'nyss_session_show_video' => array(
      'arguments' => array(),
      'template' => 'nyss-session-show-video',
    ),
  );
}

/**
 * Form builder. Configure settings for handling video offline and high-traffic conditions.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function nyss_session() {
  $form['nyss_session_status'] = array(
    '#type' => 'radios',
    '#title' => t('The senate is currently'),
    '#options' => array(
      t('Use LBDC feed'),
      t('In session'), 
      t('Not in session'),
    ),
    '#default_value' => variable_get('nyss_session_status', 0),
    '#description' => t('Normally we should use the LBDC feed, but if it isn\'t accurate, you can override it here.'),
  );
  return system_settings_form($form);
}

/**
 * Wrapper function to retrieve value of the nyss_session_status variable, which
 * specifies whether to use the old LBDC feed or manually override it to set the session status display block.
 *
 * Note by Sheldon, 2/16/2010: I'm not sure that this function is actually used, and it will probably become
 * obsolete after implementing the new journal clerk system.
 */
function nyss_session_status() {
  return (variable_get('nyss_session_status', 0) == 0);
}

/**
 * Wrapper function for nyss_session_get_active. This only exists for backwards 
 * compatibility with an old function that was improperly named. (Blame Craig or Advomatic.)
 * This is part of the old journal clerk system that is slated to be phased out.
 */
function get_session_active() {
  return nyss_session_get_active();
}

/*
 * Get session active status
 * This is part of the old journal clerk system that is slated to be phased out.
 */
function nyss_session_get_active() {
  static $senses;
    switch (variable_get('nyss_session_status', NYSS_USE_LDBC_FEED)) {
      case NYSS_IN_SESSION:
        return 'CONVENED';
        break;
      case NYSS_NOT_IN_SESSION:
        return '';
        break;
      case NYSS_USE_LDBC_FEED:
      default:
        if ($senses == '') {
          $senses = cache_get('active_session')->data;
          //dpm('cached senses: '. $senses);
          if ($senses == '') {
            $file = 'http://63.118.56.3/lbdcinfo/senses.xml';
            $xml_senses = _nyss_session_get_xml($file);
            if ($xml_senses) {
              $senses = (string)$xml_senses->channel->item->status;
              if(date('D') != 'Sat' && date('D') != 'Sun') {
                if(date('G') > 8 && date('G') < 19) {
                  cache_set('active_session', (string) $senses, 'cache', time() + 120);
                }
              }
              else {
                cache_set('active_session', (string) $senses, 'cache', time() + 900);
              }
            }
            else {
              watchdog('nyss', '"Active Session" XML file (senses.xml) not loading.', array(), 3, $file);
            }
          }
        }
        if (!$senses) {
          return 'UNDEFINED';
        }
        return $senses;
        break;
    }
}

/**
 * Wrapper function for nyss_session_get_next. This only exists for backwards 
 * compatibility with an old function that was improperly named. (Blame Craig or Advomatic.)
 * This is part of the old journal clerk system that is slated to be phased out.
 */
function get_next_session() {
  return nyss_session_get_next();
}

/*
 * Get next session
 * This is part of the old journal clerk system that is slated to be phased out.
 */
function nyss_session_get_next() {
  static $wts;
  if ($wts == '') {
    $wts = cache_get('next_session')->data;
    if (!$wts) {
      $file = 'http://63.118.56.3/wts.xml';
      $xml_wts = _nyss_session_get_xml($file);
      if ($xml_wts) {
        if ((string)$xml_wts->channel->item->startstringday != '') {
          $wts  = 'at '. (string)$xml_wts->channel->item->startstringtime;
          $wts .= ' on ' . (string)$xml_wts->channel->item->startstringday;
        }
        else {
          $wts  = ' on '. (string)$xml_wts->channel->item->startstringtime;
        }
        if(date('D') != 'Sat' && date('D') != 'Sun') {
          if(date('G') > 8 && date('G') < 19) {
            cache_set('next_session', (string) $wts, 'cache', time() + 120);
          }
        }
        else {
          cache_set('next_session', (string) $wts, 'cache', time() + 900);
        }
      }
      else {
        watchdog('nyss', '"Next session" XML file (wts.xml) not loading.', array(), 3, $file);
      }
    }
  }
  if (!$wts) {
    return t('(No time specified)');
  }
  return $wts;
}

/**
 * Returns an array with status information retrieved from the Journal Clerk's RSS feed.
 *
 * @return
 *   An array with the following keys:
 *     state          => a text string representation of the current state (e.g., 'In Session', etc.)
 *     text           => freeform text
 *     start_day      => the start day of session (in what date format?)
 *     start_time     => the start time of session (in what time format?)
 *     display_video  => a Boolean 
 *     message        => a message to display in the live_session block
 *     headline       => the headline that should display in the live_session block
 */
function nyss_session_get_status() {
  $result = _nyss_session_get_xml("http://63.118.56.3/wtsstatus.xml");
  $status = array();
  // Parse out values from the retrieved XML
  $status['state'] = (string)$result->channel->item->wtsstate;
  $status['text'] = (string)$result->channel->item->wtstext;
  $status['start_day'] = (string)$result->channel->item->startstringday;
  $status['start_time'] = (string)$result->channel->item->startstringtime;

  // Calculate a boolean value
  $status['display_video'] = in_array(trim(strtolower($status['state'])), 
    array(
      "standby to convene", 
      "standby to reconvene", 
      "in session", 
      "senate and assembly convened in joint session",
    )
  );
  
  // If freeform text has been provided, display the freeform as the message.
  
  // Otherwise, construct the message and headline according to the following rules.
  switch (trim(strtolower($status['state']))) {
    case 'standby to convene':
      $status['message'] = t('The Senate is standing by to convene.');
      $status['headline'] = t('Senate Status');
      break;
    case 'standby to reconvene':
      $status['message'] = t('The Senate is standing by to reconvene.');
      $status['headline'] = t('Senate Status');
      break;
    case 'in session':
      $status['message'] = '';
      $status['headline'] = t('Live Session');
      break;
    case 'senate and assembly convened in joint session':
      $status['message'] = '';
      $status['headline'] = t('Live Session');
      break;
    case 'temporarily at ease':
    case 'at ease':
      $status['message'] = t('The Senate is at ease.');
      $status['headline'] = t('Senate Status');
      break;
    case 'adjourned to the call of the temporary president':
      $status['message'] = t('The Senate is adjourned to the call of the Temporary President.');
      $status['headline'] = t('When is Session?');
      break;
    case 'adjourned to [date, time]':
      $status['message'] = t('The Senate is adjourned until @time, @date.', array('@date' => $status['start_day'], '@time' => $status['start_time']));
      $status['headline'] = t('When is Session?');
      break;
    case 'in recess until [time]':
    case 'in recess':
      $status['message'] = t('The Senate is in recess.');
      $status['headline'] = t('When is Session?');
      break;
    default:
      $status['message'] = t('Unknown');
      $status['headline'] = t('When is Session?');
      // belts and braces logic to make sure the temporary president case gets matched correctly
      if (substr($status['state'], 0, 12) == 'Adjourned to' && preg_match('/temporary president/i', $status['state'], $matches)) {
        $status['message'] = t('The Senate is adjourned to the call of the Temporary President.');
        $status['headline'] = t('When is Session?');
      }
      else if (substr($status['state'], 0, 12) == 'Adjourned to' && trim($status['start_day']) != '' && trim($status['start_time']) != '') {
        $status['message'] = t('The Senate is adjourned until @time, @date.', array('@date' => $status['start_day'], '@time' => $status['start_time']));
      }
      else if (substr($status['state'], 0, 15) == 'In Recess until' && $status['start_time'] != '') {
        $status['message'] = t('The Senate is in recess until @time .', array('@time' => $status['start_time']));
      }
      else if ($status['state'] != '') {
        $status['message'] = $status['state'];
      }
      else if ($status['text']) {
        $status['message'] = $status['text'];
        $status['headline'] = t('Senate Status');
      }
      break;
  }
  return $status;
}

/**
 * Retrieves some XML-formatted data.
 *
 * @param $url
 *   The URL for the data source.
 * @return
 *   A SimpleXML object based on the XML that was retrieved.
 */
function _nyss_session_get_xml($url) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
  curl_setopt($ch, CURLOPT_TIMEOUT, 15);
  $data = curl_exec($ch);
  curl_close($ch);
  return simplexml_load_string($data);
}
